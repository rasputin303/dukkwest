module Sprites
    exposing
        ( anotherGo
        , playerDown
        , playerLeft
        , playerRight
        , pudding
        , robinDown
        , robinLeft
        , robinRight
        , santa
        , title
        , tree
        , youAreDead
        )

import Types exposing (Sprite, Vec2)


playerLeft : Sprite
playerLeft =
    { path = .leftwardsPlayer
    , size = Vec2 15 30
    , hitboxSize = Vec2 15 20
    , hitboxOffset = Vec2 0 5
    }


playerRight : Sprite
playerRight =
    { path = .rightwardsPlayer
    , size = Vec2 15 30
    , hitboxSize = Vec2 15 20
    , hitboxOffset = Vec2 0 5
    }


playerDown : Sprite
playerDown =
    { path = .downwardsPlayer
    , size = Vec2 10 30
    , hitboxSize = Vec2 10 20
    , hitboxOffset = Vec2 0 5
    }


robinLeft : Sprite
robinLeft =
    { path = .leftwardsRobin
    , size = Vec2 15 30
    , hitboxSize = Vec2 15 15
    , hitboxOffset = Vec2 0 7.5
    }


robinRight : Sprite
robinRight =
    { path = .rightwardsRobin
    , size = Vec2 15 30
    , hitboxSize = Vec2 15 15
    , hitboxOffset = Vec2 0 7.5
    }


robinDown : Sprite
robinDown =
    { path = .downwardsRobin
    , size = Vec2 10 30
    , hitboxSize = Vec2 10 15
    , hitboxOffset = Vec2 0 7.5
    }


tree : Sprite
tree =
    { path = .tree
    , size = Vec2 30 30
    , hitboxSize = Vec2 10 10
    , hitboxOffset = Vec2 0 10
    }


pudding : Sprite
pudding =
    { path = .pudding
    , size = Vec2 30 30
    , hitboxSize = Vec2 30 20
    , hitboxOffset = Vec2 0 5
    }


santa : Sprite
santa =
    { path = .santa
    , size = Vec2 15 32
    , hitboxSize = Vec2 15 20
    , hitboxOffset = Vec2 0 5
    }


title : Sprite
title =
    { path = .title
    , size = Vec2 100 200
    , hitboxSize = Vec2 0 0
    , hitboxOffset = Vec2 0 0
    }


youAreDead : Sprite
youAreDead =
    { path = .dead
    , size = Vec2 101 50
    , hitboxSize = Vec2 0 0
    , hitboxOffset = Vec2 0 0
    }


anotherGo : Sprite
anotherGo =
    { path = .anotherGo
    , size = Vec2 101 22
    , hitboxSize = Vec2 0 0
    , hitboxOffset = Vec2 0 0
    }
