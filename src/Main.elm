module Main exposing (main)

import AnimationFrame
import Html exposing (Html, text)
import Keyboard.KeySet as KeySet exposing (KeySet)
import Logic
import Renderer
import Time
import Types exposing (Entity, EntityClass(..), Flags, Model, Msg(..), RenderProperties, RobinType(..), Sprite, Vec2, View(..))


main : Program Flags Model Msg
main =
    Html.programWithFlags
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


init : Flags -> ( Model, Cmd msg )
init flags =
    ( Logic.startNewGame flags
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ KeySet.subscriptions
            (\msg ->
                case msg of
                    KeySet.Down key ->
                        KeyDown key

                    KeySet.Up key ->
                        KeyUp key
            )
        , AnimationFrame.times Tick
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Tick time ->
            let
                newModel =
                    if time > model.tick + (30 * Time.millisecond) then
                        case model.view of
                            Title ->
                                model
                                    |> Logic.handleKeyboardInput

                            Game ->
                                model
                                    |> Logic.refreshRandoms
                                    |> Logic.handleKeyboardInput
                                    |> Logic.updateVelocities
                                    |> Logic.updatePositions
                                    |> Logic.updateCameraPosition
                                    |> Logic.checkCollisions
                                    |> Logic.updateScore
                                    |> Logic.updateTick time

                            LoseLife int ->
                                model
                                    |> Logic.handleKeyboardInput
                                    |> Logic.updateTick time

                            GameOver int ->
                                model
                                    |> Logic.handleKeyboardInput
                                    |> Logic.updateTick time
                    else
                        model
            in
            ( newModel
            , Cmd.none
            )

        KeyDown key ->
            ( { model | keys = KeySet.insert key model.keys }
            , Cmd.none
            )

        KeyUp key ->
            ( { model | keys = KeySet.remove key model.keys }
            , Cmd.none
            )


view : Model -> Html msg
view model =
    Renderer.screen model
        (case model.view of
            Title ->
                [ Renderer.title model ]

            Game ->
                [ [ Renderer.background model ]
                , Renderer.entities model
                , [ Renderer.lives model ]
                , [ Renderer.score model ]
                ]

            LoseLife radius ->
                [ [ Renderer.background model ]
                , Renderer.entities model
                , [ Renderer.lives model ]
                , [ Renderer.score model ]
                , [ Renderer.deathCircle radius model ]
                ]

            GameOver radius ->
                [ [ Renderer.background model ]
                , Renderer.entities model
                , [ Renderer.lives model ]
                , [ Renderer.score model ]
                , [ Renderer.deathCircle radius model ]
                , Renderer.gameOver model
                ]
        )
