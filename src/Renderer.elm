module Renderer exposing (background, deathCircle, entities, gameOver, lives, score, screen, title)

import Helpers
import Html exposing (Html)
import Sprites
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Keyed
import Types exposing (Entity, Model, Sprite, SpriteRecord, Vec2)


renderImage : Vec2 -> Vec2 -> String -> Svg msg
renderImage position size name =
    image
        [ x <| toString position.x
        , y <| toString position.y
        , width <| toString <| size.x
        , height <| toString <| size.y
        , xlinkHref <| name
        , imageRendering "pixelated"
        ]
        []


title : { a | cameraPosition : Vec2, screenWidth : Float, screenHeight : Float, scale : Float, sprites : SpriteRecord } -> List (Svg msg)
title { screenWidth, screenHeight, cameraPosition, scale, sprites } =
    let
        titleCoords =
            Helpers.spriteCoordinates (Vec2 cameraPosition.x (screenHeight / 2)) 2 Sprites.title
    in
    [ rect
        [ width <| toString screenWidth
        , height <| toString screenHeight
        , fill "black"
        ]
        []
    , renderImage
        (Vec2 titleCoords.left titleCoords.top)
        (Vec2 titleCoords.width titleCoords.height)
        (sprites |> .path Sprites.title)
    ]


gameOver : { a | cameraPosition : Vec2, screenHeight : Float, scale : Float, sprites : SpriteRecord } -> List (Svg msg)
gameOver model =
    let
        deadCoords =
            Helpers.spriteCoordinates (Vec2 model.cameraPosition.x ((model.screenHeight / 2) - 50)) model.scale Sprites.youAreDead

        anotherGoCoords =
            Helpers.spriteCoordinates (Vec2 model.cameraPosition.x ((model.screenHeight / 2) + 50)) model.scale Sprites.anotherGo
    in
    [ renderImage
        (Vec2 deadCoords.left deadCoords.top)
        (Vec2 deadCoords.width deadCoords.height)
        (.path Sprites.youAreDead model.sprites)
    , renderImage
        (Vec2 anotherGoCoords.left anotherGoCoords.top)
        (Vec2 anotherGoCoords.width anotherGoCoords.height)
        (.path Sprites.anotherGo model.sprites)
    ]


screen : { a | screenHeight : Float, screenWidth : Float } -> List (List (Svg msg)) -> Html msg
screen model components =
    svg
        [ width <| toString model.screenWidth
        , height <| toString model.screenHeight
        ]
        (List.concat components)


score : { a | score : Float } -> Svg msg
score { score } =
    Svg.text_ [ x "10", y "50", width "100", height "50", fill "black" ] [ Svg.text ("Score: " ++ toString score) ]


lives : { a | lives : Int } -> Svg msg
lives { lives } =
    Svg.text_ [ x "10", y "30", width "100", height "50", fill "black" ] [ Svg.text ("Lives: " ++ toString lives) ]


background : { a | screenHeight : Float, screenWidth : Float } -> Svg msg
background { screenWidth, screenHeight } =
    rect
        [ width <| toString screenWidth
        , height <| toString screenHeight
        , fill "lightgray"
        ]
        []


entities : Model -> List (Svg msg)
entities ({ player, entities, cameraPosition, screenWidth, screenHeight, scale } as model) =
    let
        sortedRenderList =
            (player :: entities)
                |> List.sortBy (\entity -> entity.position.y)
    in
    List.indexedMap (renderEntity model) sortedRenderList


renderEntity : Model -> Int -> Entity -> Svg msg
renderEntity { cameraPosition, screenWidth, screenHeight, scale, sprites } index entity =
    let
        xCoord =
            entity.position.x - cameraPosition.x + (screenWidth / 2)

        yCoord =
            entity.position.y - cameraPosition.y + (screenHeight / 2)

        spriteCoords =
            Helpers.spriteCoordinates (Vec2 xCoord yCoord) scale (entity.sprite entity.velocity)

        spriteName =
            .path (entity.sprite entity.velocity) sprites

        hitbox =
            Helpers.hitBoxCoordinates entity scale
    in
    Svg.Keyed.node "svg"
        []
        [ ( "sprite_" ++ toString index
          , renderImage
                (Vec2 spriteCoords.left spriteCoords.top)
                (Vec2 spriteCoords.width spriteCoords.height)
                spriteName
          )
        , ( "hitbox_" ++ toString index
          , rect
                [ x <| toString <| hitbox.left - cameraPosition.x + (screenWidth / 2)
                , y <| toString <| hitbox.top - cameraPosition.y + (screenHeight / 2)
                , width <| toString <| hitbox.width
                , height <| toString <| hitbox.height
                , fill "none"
                ]
                []
          )
        ]


deathCircle : a -> { e | cameraPosition : Vec2, player : { d | position : Vec2 }, screenHeight : Float, screenWidth : Float } -> Svg msg
deathCircle radius model =
    let
        circleX =
            model.player.position.x - model.cameraPosition.x + (model.screenWidth / 2)

        circleY =
            model.player.position.y - model.cameraPosition.y + (model.screenHeight / 2)
    in
    circle
        [ fill "black"
        , cx <| toString <| circleX
        , cy <| toString <| circleY
        , r <| toString radius
        ]
        []
