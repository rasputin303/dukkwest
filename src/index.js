import './main.css';
import { Main } from './Main.elm';

import title from './sprites/title.png';
import anotherGo from './sprites/anotherGo.png';
import dead from './sprites/dead.png';
import downwardsPlayer from './sprites/downwardsPlayer.png';
import downwardsRobin from './sprites/downwardsRobin.png';
import leftwardsPlayer from './sprites/leftwardsPlayer.png';
import leftwardsRobin from './sprites/leftwardsRobin.png';
import rightwardsPlayer from './sprites/rightwardsPlayer.png';
import rightwardsRobin from './sprites/rightwardsRobin.png';
import pudding from './sprites/pudding.png';
import santa from './sprites/santa.png';
import tree from './sprites/tree.png';
import registerServiceWorker from './registerServiceWorker';

//Main.embed(document.getElementById('root'));

Main.fullscreen(
  {
    randomInt: Math.floor(Math.random()*0x0FFFFFFF),
    sprites: {
      title : title,
      anotherGo : anotherGo,
      dead : dead,
      downwardsPlayer : downwardsPlayer,
      downwardsRobin : downwardsRobin,
      leftwardsPlayer : leftwardsPlayer,
      leftwardsRobin : leftwardsRobin,
      rightwardsPlayer : rightwardsPlayer,
      rightwardsRobin : rightwardsRobin,
      pudding : pudding,
      santa : santa,
      tree : tree
    }
  }
);

registerServiceWorker();
