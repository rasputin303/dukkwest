module Types exposing (Entity, EntityClass(..), Flags, Model, Msg(..), RenderProperties, RobinType(..), Sprite, SpriteRecord, Vec2, View(..))

import Keyboard.Key exposing (Key(..))
import Keyboard.KeySet as KeySet exposing (KeySet)
import Random.Pcg as Pcg exposing (Seed)
import Time


type Msg
    = NoOp
    | Tick Time.Time
    | KeyDown Key
    | KeyUp Key


type alias Flags =
    { randomInt : Int
    , sprites : SpriteRecord
    }


type alias SpriteRecord =
    { title : String
    , anotherGo : String
    , dead : String
    , downwardsPlayer : String
    , downwardsRobin : String
    , leftwardsPlayer : String
    , leftwardsRobin : String
    , rightwardsPlayer : String
    , rightwardsRobin : String
    , pudding : String
    , santa : String
    , tree : String
    }


type alias Model =
    { view : View
    , player : Entity
    , lives : Int
    , entities : List Entity
    , tick : Time.Time
    , screenWidth : Float
    , screenHeight : Float
    , cameraPosition : Vec2
    , controls : Vec2
    , random : List Vec2
    , seed : Seed
    , scale : Float
    , score : Float
    , keys : KeySet
    , sprites : SpriteRecord
    }


type View
    = Title
    | Game
    | LoseLife Int
    | GameOver Int


type EntityClass
    = Player
    | Robin RobinType
    | Pudding
    | Santa
    | Tree


type RobinType
    = Rightwards
    | Leftwards
    | Downwards


type alias Vec2 =
    { x : Float
    , y : Float
    }


type alias Entity =
    { type_ : EntityClass
    , sprite : Vec2 -> Sprite
    , position : Vec2
    , velocity : Vec2
    , minVelocity : Vec2
    , maxVelocity : Vec2
    , acceleration : Vec2
    }


type alias Sprite =
    { path : SpriteRecord -> String
    , size : Vec2
    , hitboxSize : Vec2
    , hitboxOffset : Vec2
    }


type alias RenderProperties a =
    { a
        | screenWidth : Float
        , screenHeight : Float
        , cameraPosition : Vec2
        , scale : Float
    }
