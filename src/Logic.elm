module Logic exposing (checkCollisions, handleKeyboardInput, refreshRandoms, startNewGame, startNewLife, updateCameraPosition, updatePositions, updateScore, updateTick, updateVelocities)

import Entities
import Helpers
import Keyboard.Key exposing (Key(..))
import Keyboard.KeySet as KeySet exposing (KeySet)
import Random.Pcg as Pcg exposing (Seed)
import Time
import Types exposing (Entity, EntityClass(..), Flags, Model, RenderProperties, RobinType(..), Vec2, View(..))


startNewGame : Flags -> Model
startNewGame flags =
    let
        screenWidth =
            800
    in
    { view = Title
    , player = Entities.player { screenWidth = screenWidth }
    , lives = 3
    , entities = makeEntityList [ ( Entities.tree, 8 ) ]
    , tick = 0
    , screenWidth = screenWidth
    , screenHeight = 3 / 4 * screenWidth
    , cameraPosition = Vec2 400 300
    , controls = Vec2 0 0
    , random = []
    , seed = Pcg.initialSeed flags.randomInt
    , scale = 2
    , score = 0
    , keys = KeySet.empty
    , sprites = flags.sprites
    }
        |> refreshRandoms
        |> updatePositions


startNewLife : Model -> Model
startNewLife model =
    let
        randomInt =
            Pcg.step (Pcg.int 0 1) model.seed
                |> Tuple.first

        newModel =
            startNewGame { randomInt = randomInt, sprites = model.sprites }
                |> (\x -> { x | cameraPosition = Vec2 400 model.player.position.y })

        player =
            model.player
    in
    { newModel
        | lives = model.lives
        , score = model.score
        , player = { player | position = Vec2 400 model.player.position.y }
        , entities = List.map2 (randomizePosition model) model.random model.entities
        , view = Game
    }


makeEntityList : List ( Entity, Int ) -> List Entity
makeEntityList specifications =
    specifications
        |> List.map (\( entity, number ) -> List.map (\_ -> entity) (List.range 1 number))
        |> List.concat


handleKeyboardInput : Model -> Model
handleKeyboardInput model =
    let
        when this that otherwise =
            if KeySet.member this model.keys then
                that
            else
                otherwise
    in
    case model.view of
        Title ->
            when Space
                { model | view = Game }
                model

        Game ->
            let
                x =
                    when ArrowLeft -1 0 + when ArrowRight 1 0

                y =
                    when Space 0 1
            in
            { model | controls = Vec2 x y }

        LoseLife _ ->
            when Space (startNewLife model) model

        GameOver _ ->
            let
                randomInt =
                    Pcg.step (Pcg.int 0 1) model.seed
                        |> Tuple.first
            in
            when Space
                (startNewGame { randomInt = randomInt, sprites = model.sprites })
                model


randomizePosition : Model -> Vec2 -> Entity -> Entity
randomizePosition { cameraPosition, screenWidth, screenHeight } random entity =
    let
        randomX =
            Helpers.scaleNumber 0 screenWidth random.x

        randomY =
            Helpers.scaleNumber 0 screenHeight random.y

        position =
            case entity.type_ of
                Tree ->
                    Vec2 randomX (cameraPosition.y + (screenHeight * 0.5) + randomY)

                Robin Rightwards ->
                    Vec2 -15 (cameraPosition.y - (screenHeight * 0.5) + randomY)

                Robin Leftwards ->
                    Vec2 (screenWidth + 15) (cameraPosition.y - (screenHeight * 0.5) + randomY)

                Robin Downwards ->
                    Vec2 randomX (cameraPosition.y - (0.5 * screenHeight) - 250)

                Pudding ->
                    Vec2 randomX (cameraPosition.y - (0.5 * screenHeight) - 250)

                Santa ->
                    Vec2 randomX (cameraPosition.y + (screenHeight * 0.5) + randomY)

                _ ->
                    entity.position
    in
    { entity | position = position }


refreshRandoms : Model -> Model
refreshRandoms model =
    let
        generator _ ( sd, rs ) =
            let
                ( ( x, y ), newSeed ) =
                    getRandomPair sd
            in
            ( newSeed, Vec2 x y :: rs )

        ( seed, random ) =
            List.foldl generator ( model.seed, [] ) model.entities
    in
    { model | random = random, seed = seed }


updateCameraPosition : Model -> Model
updateCameraPosition model =
    { model
        | cameraPosition =
            if model.player.position.y > 0.5 * model.screenHeight then
                Vec2 model.cameraPosition.x model.player.position.y
            else
                model.cameraPosition
    }


getRandomPair : Seed -> ( ( Float, Float ), Seed )
getRandomPair seed =
    Pcg.step (Pcg.pair (Pcg.float 0 1) (Pcg.float 0 1)) seed


updateScore : Model -> Model
updateScore model =
    let
        oldScore =
            model.score

        newScore =
            model.score + model.player.velocity.y

        threshold score =
            oldScore < score && newScore >= score

        ( ( x, y ), seed ) =
            getRandomPair model.seed

        newEntities =
            if threshold 2000 then
                randomizePosition model (Vec2 x y) Entities.rightwardsRobin :: model.entities
            else if threshold 4000 then
                randomizePosition model (Vec2 x y) Entities.leftwardsRobin :: model.entities
            else if threshold 8000 then
                randomizePosition model (Vec2 x y) Entities.downwardsRobin :: model.entities
            else if threshold 12000 then
                randomizePosition model (Vec2 x y) Entities.pudding :: model.entities
            else if threshold 15000 then
                randomizePosition model (Vec2 x y) Entities.santa :: model.entities
            else
                model.entities
    in
    { model | score = newScore, entities = newEntities, seed = seed }


updateVelocities : Model -> Model
updateVelocities model =
    let
        ( ( randomX, randomY ), seed ) =
            getRandomPair model.seed

        behaviour entity =
            case entity.type_ of
                Player ->
                    model.controls

                Tree ->
                    Vec2 0 0

                Robin Downwards ->
                    if entity.position.x < model.player.position.x then
                        Vec2 2 1
                    else if entity.position.x > model.player.position.x then
                        Vec2 -2 1
                    else
                        Vec2 0 1

                Pudding ->
                    Vec2
                        (Helpers.scaleNumber -20 20 randomX)
                        (Helpers.scaleNumber -25 10 randomY)

                _ ->
                    Vec2 0 0
    in
    { model
        | entities = List.map (\x -> newVelocity (behaviour x) x) model.entities
        , player = newVelocity (behaviour model.player) model.player
        , seed = seed
    }


newVelocity : Vec2 -> Entity -> Entity
newVelocity diff entity =
    let
        xLimit =
            clamp entity.minVelocity.x entity.maxVelocity.x

        yLimit =
            clamp entity.minVelocity.y entity.maxVelocity.y

        newVelocityX =
            if diff.x > 0 then
                xLimit (entity.velocity.x + entity.acceleration.x)
            else if diff.x < 0 then
                xLimit (entity.velocity.x - entity.acceleration.x)
            else
                entity.velocity.x

        newVelocityY =
            if diff.y > 0 then
                yLimit (entity.velocity.y + entity.acceleration.y)
            else if diff.y < 0 then
                yLimit (entity.velocity.y - entity.acceleration.y)
            else
                entity.velocity.y
    in
    { entity | velocity = Vec2 newVelocityX newVelocityY }


updatePositions : Model -> Model
updatePositions model =
    { model
        | entities = List.map2 (newPosition model) model.random model.entities
        , player = newPosition model (Vec2 0 0) model.player
    }


newPosition : Model -> Vec2 -> Entity -> Entity
newPosition ({ screenWidth, screenHeight, cameraPosition } as model) random entity =
    let
        provisionalPosition =
            Vec2
                (entity.position.x + entity.velocity.x)
                (entity.position.y + entity.velocity.y)

        offScreen =
            isOffScreen model entity
    in
    case entity.type_ of
        Player ->
            { entity
                | position =
                    Vec2
                        (clamp 0 screenWidth provisionalPosition.x)
                        provisionalPosition.y
            }

        Tree ->
            if offScreen Top then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }

        Robin Rightwards ->
            if offScreen Right || offScreen Bottom || offScreen Top then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }

        Robin Leftwards ->
            if offScreen Left || offScreen Bottom || offScreen Top then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }

        Robin Downwards ->
            if offScreen Bottom || offScreen Left || offScreen Right then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }

        Pudding ->
            if offScreen Bottom || offScreen Left || offScreen Right then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }

        Santa ->
            if offScreen Top then
                randomizePosition model random entity
            else
                { entity | position = provisionalPosition }


isOffScreen : RenderProperties a -> Entity -> OffscreenType -> Bool
isOffScreen renderProperties entity direction =
    List.member direction (checkOffScreen renderProperties entity)


checkOffScreen : RenderProperties a -> Entity -> List OffscreenType
checkOffScreen { screenWidth, screenHeight, cameraPosition, scale } entity =
    let
        sprite =
            Helpers.spriteCoordinates entity.position scale (entity.sprite entity.velocity)
    in
    List.filterMap identity
        [ if sprite.right < cameraPosition.x - (0.5 * screenWidth) then
            Just Left
          else
            Nothing
        , if sprite.left > cameraPosition.x + (0.5 * screenWidth) then
            Just Right
          else
            Nothing
        , if sprite.bottom < cameraPosition.y - (0.5 * screenHeight) then
            Just Top
          else
            Nothing
        , if sprite.top > cameraPosition.y + (0.5 * screenHeight) then
            Just Bottom
          else
            Nothing
        ]


type OffscreenType
    = Left
    | Right
    | Top
    | Bottom


checkCollisions : Model -> Model
checkCollisions model =
    let
        playerHitbox =
            Helpers.hitBoxCoordinates model.player model.scale

        isCollision entity =
            let
                entityHitbox =
                    Helpers.hitBoxCoordinates entity model.scale
            in
            (playerHitbox.right > entityHitbox.left)
                && (playerHitbox.left < entityHitbox.right)
                && (playerHitbox.top < entityHitbox.bottom)
                && (playerHitbox.bottom > entityHitbox.top)
    in
    if List.any isCollision model.entities && model.view == Game then
        killPlayer model
    else
        model


killPlayer : Model -> Model
killPlayer ({ view, lives } as model) =
    let
        newLives =
            lives - 1

        newView =
            case newLives of
                0 ->
                    GameOver 0

                _ ->
                    LoseLife 0
    in
    { model | view = newView, lives = newLives }


updateTick : Time.Time -> Model -> Model
updateTick time model =
    { model
        | tick = time
        , view =
            case model.view of
                LoseLife radius ->
                    LoseLife (radius + 30)

                GameOver radius ->
                    GameOver (radius + 30)

                otherView ->
                    otherView
    }
